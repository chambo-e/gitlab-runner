package s3

import (
	"errors"
	"testing"

	"github.com/minio/minio-go"
	"github.com/minio/minio-go/pkg/credentials"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlab-runner/common"
)

func insecureCacheFactory() *common.CacheConfig {
	cacheConfig := defaultCacheFactory()
	cacheConfig.S3.Insecure = true

	return cacheConfig
}

func s3v2Cachefactory() *common.CacheConfig {
	cacheConfig := defaultCacheFactory()
	cacheConfig.S3.SignatureVersion = "s3v2"

	return cacheConfig
}

func emptyCredentialsCacheFactory() *common.CacheConfig {
	cacheConfig := defaultCacheFactory()
	cacheConfig.S3.ServerAddress = ""
	cacheConfig.S3.AccessKey = ""
	cacheConfig.S3.SecretKey = ""

	return cacheConfig
}

type minioClientInitializationTest struct {
	errorOnInitialization bool
	configuractionFactory func() *common.CacheConfig

	expectedToUseIAM        bool
	expectedInsecure        bool
	expectedSignatureTypeV2 bool
}

func runOnFakeMinioWithCredentials(t *testing.T, test minioClientInitializationTest) func() {
	oldNewMinio := newMinioWithCredentials
	newMinioWithCredentials = func(endpoint string, credentials *credentials.Credentials, secure bool, region string) (*minio.Client, error) {
		if test.errorOnInitialization {
			return nil, errors.New("test error")
		}

		if test.expectedToUseIAM {
			assert.Equal(t, "s3.amazonaws.com", endpoint)
			assert.True(t, secure)
			assert.Empty(t, region)
		} else {
			if test.expectedInsecure {
				assert.False(t, secure)
			} else {
				assert.True(t, secure)
			}

			creds, err := credentials.Get()
			require.NoError(t, err)
			if test.expectedSignatureTypeV2 {
				assert.True(t, creds.SignerType.IsV2())
			} else {
				assert.True(t, creds.SignerType.IsV4())
			}
		}

		client, err := minio.NewWithCredentials(endpoint, credentials, secure, region)
		require.NoError(t, err)

		return client, nil
	}

	return func() {
		newMinioWithCredentials = oldNewMinio
	}
}

func TestMinioClientInitialization(t *testing.T) {
	tests := map[string]minioClientInitializationTest{
		"error-on-initialization": {
			errorOnInitialization: true,
			configuractionFactory: defaultCacheFactory,
		},
		"should-use-IAM": {
			configuractionFactory: emptyCredentialsCacheFactory,
			expectedToUseIAM:      true,
		},
		"should-use-explicit-credentials": {
			configuractionFactory: defaultCacheFactory,
		},
		"should-use-explicit-credentials-with-insecure": {
			configuractionFactory: insecureCacheFactory,
			expectedInsecure:      true,
		},
		"should-use-explicit-credentials-with-s3v2": {
			configuractionFactory:   s3v2Cachefactory,
			expectedSignatureTypeV2: true,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			cleanupMinioCredentialsMock := runOnFakeMinioWithCredentials(t, test)
			defer cleanupMinioCredentialsMock()

			cacheConfig := test.configuractionFactory()
			client, err := newMinioClient(cacheConfig.S3)

			if test.errorOnInitialization {
				assert.Error(t, err, "test error")
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, client)
		})
	}
}
